package com.example.orderservice.controller;

import com.example.orderservice.dto.OrderDto;
import com.example.orderservice.entity.Order;
import com.example.orderservice.service.OrderService;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/orders")
public class OrderController {
  private OrderService orderService;

  public OrderController(OrderService orderService) {
    this.orderService = orderService;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Order addOrder(@RequestBody OrderDto orderDto) {
    return orderService.addOrder(orderDto);
  }

  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  public Order getOrder(@RequestParam("id") Long orderId) {
    return orderService.getOrder(orderId);
  }

  @GetMapping("/userOrders")
  public List<Order> gerOrders(@RequestParam("user_id") Long userId) {
    return orderService.getUserOrders(userId);
  }

  @DeleteMapping
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteOrder(@RequestParam("id") Long orderId) {
    orderService.deleteOrder(orderId);
  }
}
