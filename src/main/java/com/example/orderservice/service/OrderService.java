package com.example.orderservice.service;

import com.example.orderservice.dto.GoodDto;
import com.example.orderservice.dto.OrderDto;
import com.example.orderservice.entity.Good;
import com.example.orderservice.entity.Order;
import com.example.orderservice.repository.OrderRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class OrderService {
  private OrderRepository orderRepository;

  public OrderService(OrderRepository orderRepository) {
    this.orderRepository = orderRepository;
  }

  public Order addOrder(OrderDto orderDto) {
    List<GoodDto> goodDtos = orderDto.getGoodDtos();
    List<Good> goods = goodDtos.stream()
        .map(goodDto -> Good.builder()
            .bicycleId(goodDto.getBicycleId())
            .amount(goodDto.getAmount())
            .unitGoodPrice(goodDto.getUnitGoodPrice())
            .totalGoodPrice(goodDto.getUnitGoodPrice() * goodDto.getAmount())
            .build())
        .collect(Collectors.toList());
    Order order = Order.builder()
        .userId(orderDto.getUserId())
        .goods(goods)
        .totalOrderPrice(goods.stream()
            .mapToDouble(Good::getTotalGoodPrice)
            .sum())
        .build();
    goods.forEach(good -> good.setOrder(order));
    return orderRepository.save(order);
  }

  public Order getOrder(Long orderId) {
    return orderRepository.findById(orderId).get();
  }

  public void deleteOrder(Long orderId) {
    orderRepository.deleteById(orderId);
  }

  public List<Order> getUserOrders(Long userId) {
    return orderRepository.findByUserId(userId);
  }
}
