create table bicycle_order
(
    id                  BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'ORDERID',
    user_id              BIGINT NOT NULL COMMENT 'USERID',
    total_order_price     DOUBLE NOT NULL COMMENT '订单总价'
);