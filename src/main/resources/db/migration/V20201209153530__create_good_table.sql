create table good
(
    id                  BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'ORDERID',
    bicycle_id          BIGINT NOT NULL COMMENT '单车类型ID',
    amount              INT NOT NULL COMMENT  '数量',
    unit_good_price     DOUBLE NOT NULL COMMENT '单价',
    total_good_price    DOUBLE NOT NULL COMMENT '总价',
    order_id            BIGINT NOT NULL COMMENT 'orderID',
    FOREIGN KEY (order_id) REFERENCES bicycle_order(id)
);